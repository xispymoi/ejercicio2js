// En base al diámetro que reciba la función se comportará así:

// - 1) Si el diámetro es **inferior o igual** a 10, la función deberá imprimir por consola "es una rueda para un juguete pequeño"
// - 2) Si el diámetro es **superior** a 10 y **menor** de 20, la función deberá imprimir por consola "es una rueda para un juguete mediano"
// - 3) Si el diámetro es **superior o igual** a 20, la función deberá imprimir por consola "es una rueda para un juguete grande"

// Ejecuta varias veces tu función con distintos valores, por ejemplo: 5, 7, 14, 31... y comprueba que los resultados son correctos. Recuerda que puedes realizar el ejercicio en un fichero *ejercicio2.js*.


function wheel (sizeOne) {
if (sizeOne <= 10) {
    console.log ("es una rueda para un juguete pequeño");
} 

 else if (sizeOne > 10 && sizeOne < 20) {
    console.log ("es una rueda para un juguete mediano");
}

else if (sizeOne >= 20) {
    console.log ("es una rueda para un juguete grande");
} 
}
var result= wheel(15);

console.log (result);



